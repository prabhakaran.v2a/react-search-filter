import React, {useState, useEffect} from 'react'
const CardList = (props) => {
    let pattern  = new RegExp(`${props.searchMatch}`,"i");
    let content = document.querySelectorAll("span");
    const [itemFound, setItemFound] = useState("");
    useEffect(()=>{
        filterList();
        searchItem();
    },[props.searchMatch])

    const filterList = () => {
        content.forEach((x)=>{
            x.innerHTML = x.textContent.replace(pattern, match => `<mark>${match}</mark>`)
        })
    }
    const searchItem = () => {
        let item = props?.cardData.items.filter((data)=> data.toLowerCase() === props.searchMatch.toLowerCase())
        if(item.length !==0 ){
            setItemFound(item[0])
        }
        else if(props?.cardData.id.toLowerCase() === props.searchMatch.toLowerCase()){
            setItemFound(props?.cardData.id)
        }
        else if(props?.cardData.name.toLowerCase() === props.searchMatch.toLowerCase()){
            setItemFound(props?.cardData.name)
        }
        else if(props?.cardData.address.toLowerCase() === props.searchMatch.toLowerCase()){
            setItemFound(props?.cardData.address)
        }
        else if(props?.cardData.pincode.toLowerCase() === props.searchMatch.toLowerCase()){
            setItemFound(props?.cardData.pincode)
        }
        else {
            setItemFound("")
        }
    }
    let { id, name, address, pincode, items } = props?.cardData;
    return (
        <div className="card">
            ID: : <span>{id}</span><br/>
            Name : <span>{name}</span><br/>
            Address : <span>{address}</span><br/>
            
            
            <div className="items"> 
            Items :
                {items.map((item)=> (
                    <span >{item}</span>
                ))}
            </div> 
            Pin Code: <span>{pincode}</span><br/>
            {itemFound !== "" ? (
                <span>{`"${itemFound}" found in items`}</span>
            ): <></>}
        </div>

    )
}

export default CardList
