import React, { useRef} from 'react'
import CardList from './CardList'

const CardListing = (props) => {
    const inputE1 = useRef("");
    const renderContactList = props.contacts.map((contact)=> (
        <CardList cardData={contact} searchMatch={props.term}/>
    ))
    const getSearchTerm = () => {
        props.searchKeyword(inputE1.current.value)
    }
    return (
        <div className='outer'>
            <div className='ui icon input'>
                <input type="text" width="100" placeholder="Search by Id, name, address, item or pincode" className="prompt" value={props.term} ref={inputE1} onChange={getSearchTerm}></input>
            </div>
            <div className="mainDev">
            {renderContactList.length > 0 ? renderContactList : "No Contact Found"}
            </div>
        </div>
    )
}

export default CardListing
