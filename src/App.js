import React, {useState, useEffect} from 'react';
import './App.css';
import CardListing from './components/CardListing';
import api from './api'
function App() {
  const [contacts, setContacts] = useState([]);
  const [searchContacts, setSearchContact] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  
  useEffect(() => {
      getCardData();
  }, [])
  const getCardData = async () => {
      const response = await api.get("/v2/5ba8efb23100007200c2750c").catch((err)=>{
          console.log(err);
      });
      setContacts(response?.data);
  }
  const searchHandler = (search) => {
    setSearchTerm(search)
    if(searchTerm !== ""){
      const newList = contacts.filter((data)=>{
          return Object.values(data).join(" ").toLowerCase().includes(searchTerm.toString().toLowerCase());
      })
      setSearchContact(newList)
    }
    else {
      setContacts(contacts)
    }
  }
  return (
    <div className="App">
      <CardListing contacts={searchTerm.length < 1 ? contacts : searchContacts} term={searchTerm} searchKeyword={searchHandler}/>
    </div>
  );
}

export default App;
